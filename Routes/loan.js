
const loanControllers = require('../Controllers/loan')
const loanValidate = require('../Utils/loanValidation')

module.exports = function (InstaLoan) {

    // To create / add Loan API for customers

    InstaLoan.post('/add/loans', function (req, res) {

        console.log("Routes: Inside '/add/loan' functionallity");

        const { error } = loanValidate.loan_reg.validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);
        loanControllers.loanCreate(req.body, function (err, result) {
            if (err) {
                res.send(err);
            } else {
                res.send(result);
            }
        });
    });

    /// To get the loan details API for customers

    InstaLoan.get('/list/loans', function (req, res) {

        const { error } = loanValidate.loan_Valist.validate(req.query);
        if (error) return res.status(400).send(error.details[0].message);
        loanControllers.loanList(req.query, function (err, result) {
            if (err) {
                res.send(err);
            } else {
                res.send(result);
            }
        })
    });

    // To update the loan details API for customers

    InstaLoan.put('/update/loans', function (req, res) {
        const { error } = loanValidate.loan_Valupdate.validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        loanControllers.updateLoan(req.body, function (err, result) {
            if (err) {
                res.send(err);
            } else {
                res.send(result);
            }
        })

    });
    // To Delete the loan details API for customers
    InstaLoan.delete('/delete/loans', function (req, res) {
        console.log("Routes/loan: Inside 'delete/loan' API");

        const { error } = loanValidate.loan_Valdelete.validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);
        loanControllers.deleteLoan(req.body, function (err, result) {
            if (err) {
                res.send(err);
            } else {
                res.send(result);
            }
        });
    });

}

