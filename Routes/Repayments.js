const controller_repayment = require('../Controllers/Repayments')
const repay_Validate = require('../Utils/RepaymentValidation')
module.exports = function (InstaLoan) {

    // Create Repayments API 

    InstaLoan.post('/create/repayment', function (req, res) {

        const { error } = repay_Validate.repay_reg.validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);
        console.log("Routes: Inside '/create/repayment' functionality");
        controller_repayment.create_Repayment(req.body, function (err, result) {
            if (err) {
                res.send(err);
            } else {
                res.send(result);
            }
        });
    });

    //list all the repayments API in Routes for customers

    InstaLoan.get('/list/repayment', function (req, res) {

        const { error } = repay_Validate.repay_list.validate(req.query);
        if (error) return res.status(400).send(error.details[0].message);
        controller_repayment.list_Repayment(req.query, function (err, result) {
            if (err) {
                res.send(err);
            } else {
                res.send(result);
            }
        });
    });
    // To update the Repayment  API in Routes for customers
    InstaLoan.put('/update/repayment', function (req, res) {

        const { error } = repay_Validate.repay_update.validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        controller_repayment.update_repayment(req.body, function (err, result) {
            if (err) {
                res.send(err);
            } else {
                res.send(result);
            }
        });
    });
    // To delete the Repayment  API in Routes for customers

    InstaLoan.delete('/delete/repayment', function (req, res) {

        const { error } = repay_Validate.repay_del.validate(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        controller_repayment.delete_document_in_DB(req.body, function (err, result) {
            if (err) {
                res.send(err);
            } else {
                res.send(result);
            }
        });
    });
}


