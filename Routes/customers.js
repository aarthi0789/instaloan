
const CustomerControllers = require('../Controllers/customer')
const validator = require('../Utils/CustomerValidation')

module.exports = function (InstaLoan) {

    /// TO create the customers data POST API
    InstaLoan.post('/add/users', function (req, res) {
        

		const  {error } = validator.customer_reg.validate(req.body);
        if(error) return res.status(400).send(error.details[0].message);

        console.log("Routes: Inside '/add/users' API  function.")
        CustomerControllers.CustController(req.body,  (err, result)=>res.send(err || result));

    });


    /// To read all the customers data in GET API

    InstaLoan.get('/list/users', function (req, res) {

        const {error} = validator.customer_list.validate(req.body);
        if(error) return res.status(400).send(error.details[0].message);
        CustomerControllers.custList( req.query,(err, result)=> res.send(err || result));


    });

    /// To update  the customers data in PUT API

    InstaLoan.put('/update/users', function (req, res) {
        console.log("Routes: Inside 'customer/update' function");
        
        // const {error}= validator.customer_update.validate(req.body);
        // if(error) return res.status(400).send(error.details[0].message);

        CustomerControllers.updateCust(req.body, function (err, result) {

            if (err) {
                res.send(err);
            } else {
                res.send(result);
            }
        })

    });

    ///// To delete the customer data in DELETE logic

    InstaLoan.delete('/delete/users', function (req, res) {
         
         const {error}= validator.customer_delete.validate(req.body);
         if(error) return res.status(400).send(error.details[0].message);

        CustomerControllers.deleteCust(req.body, function (err, result) {
            if (err) {
                res.send(err);
            } else {
                res.send(result);
            }
        });
    })

}


