const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const LoanSchema = new Schema({
    // loanId: {type: Schema.Types.ObjectId, ref: `loans`},
    LoanType: { type: String },// 3-Months, 6-Months, 12-Months
    LoanAmount: { type: Number },
    customerId: { type: Schema.Types.ObjectId, ref: `customers` },
    payment_mode: { type: String }, // Week, Month, Year
    release_date: { type: Date, default: Date.now() }
})



const Loan = mongoose.model('Loan', LoanSchema);
module.exports = Loan;