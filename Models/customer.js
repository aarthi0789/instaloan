const mongoose = require('mongoose')

const Schema = mongoose.Schema;


const customerSchema = new Schema({

    firstName: { type: String },
    lastName: { type: String },
    age: { type: Number },
    gender: { type: String },
    mobile_num: { type: String },
    emailId: { type: String },
    address: { type: String },
    occupation: { type: String },
})

const customer = mongoose.model('customer', customerSchema);
module.exports = customer;

