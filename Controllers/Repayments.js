const universalFun_repayments = require('../Utils/UniversalFunctions')
const repayment_Model = require('../Models/Repayments')


module.exports = {
    // POST logic to create repayments in controller function
    create_Repayment: function (data, callback) {

        
        queryObj = {};
        (data._id) ? queryObj._id = data._id : null;

        universalFun_repayments.saveInDB(repayment_Model, queryObj, function (err, res) {

            if (err) {
                callback(err, null);
            } else {
                callback(null, res);
            }

        });

    },
    // GET logic to get list of repayments in controller function

    list_Repayment: function (data, callback) {
        universalFun_repayments.list_documents_in_DB(repayment_Model, data, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, res);
            }
        })

    },

    // PUT  logic to update of repayments in controller function

    update_repayment: function (data, callback) {
        let queryObj = {
            loanId: data.loanId
        };
        let updateObj = {
            // loanId: data.loanId,
            // customerId: data.customerId,
            re_payment_amount: data.re_payment_amount
        };
        universalFun_repayments.update_documents_inDB(repayment_Model, queryObj, updateObj, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, res);
            }
        });
    },

    // DELETE  logic to update of repayments in controller function

    delete_repayment: function (data, callback) {

        queryObj = {
            _id: data._id
        };
        universalFun_repayments.delete_document_in_DB(repayment_Model, queryObj, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, res);
            }

        })
    }

}