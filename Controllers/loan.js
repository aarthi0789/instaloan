const UniversalFunctions = require('../Utils/UniversalFunctions')
const LoanModel = require('../Models/loan')

module.exports = {


    // Create all the loan details to customers
    loanCreate: function (data, callback) {

        console.log("Controllers: Inside 'loanCreate' functionality.");
        UniversalFunctions.saveInDB(LoanModel, data, function (err, res) {

            if (err) {
                callback(err, null);
            } else {
                callback(null, res);
            }
        });

    },


    /// Get all the customer loan list

    loanList: function (data, callback) {

        queryObj = {};
        (data._id) ? queryObj._id = data._id : null;

        UniversalFunctions.list_documents_in_DB(LoanModel, queryObj, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, res);
            }
        });
    },
    // update all the customer loan list
    updateLoan: function (data, callback) {

        console.log("Controllers: Inside 'updateLoan' functionality.");
        let queryObj = {
            _id: data._id
        };
        let updateObj = {
            LoanAmount: data.LoanAmount,
            payment_mode: data.payment_mode,
        };
        UniversalFunctions.update_documents_inDB(LoanModel, queryObj, updateObj, function (err, res) {

            if (err) {
                callback(err, null);
            } else {
                callback(null, res);
            }
        });
    },
    // Delete customer loans logic

    deleteLoan: function (data, callback) {
        queryObj = {
            _id: data._id
        };
        UniversalFunctions.delete_document_in_DB(LoanModel, queryObj, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, res);
            }
        });
    },
}