
const CustModel = require('../Models/customer')

module.exports = {

    /* methodNameforAPI: annonomusFunction(dataObject, callback function);
     */
    /// PUSH lOGIC to add customers details
    CustController: function (data, callback) {

        CustModel(data).save(function (err, res) { // .save method is to store a new document in Model
            if (err) {
                callback(err, null);
            } else {

                var resp = {
                    message: "Customer added sucessfully",
                    data: res,
                }
                callback(null, resp);
            }
        });
    },

    /// GET Method - getting user list from DB server

    custList: function (data, callback) {

        queryObj = {};
        (data._id) ? queryObj._id = data._id : null; // Ternary operator to get the list from request query()

        CustModel.find(queryObj, function (err, res) { // find method finds the user list from the DB
            if (err) {
                callback(err, null);
            } else {
                callback(null, res);
            }
        })
    },
    ///  To update  the customers data in PUT logic

    updateCust: function (data, callback) {
        let queryObj = { _id: data._id }; // requesting with _id from data 
        let updateObj = {
            // firstName: data.firstName,
            // lastName: data.lastName,
            // age: data.age,
            // gender: data.gender,
            // address: data.address,
            mobile_num: data.mobile_num,
            emailId: data.emailId,
            occupation: data.occupation
        };
        /* ModelName.findOneAndUpdate method*/
        CustModel.findOneAndUpdate(queryObj, updateObj, function (err, result) {
            if (err) {
                callback(err, null)
            } else {

                resl = {
                    message: "Customer updated sucessfully",
                    data: result,
                }
                callback(null, resl)
            }
        })
    },

    /// To delete the customer data in DELETE logic

    deleteCust: function (data, callback) {
        let queryDel = { _id: data._id };


        CustModel.deleteOne(queryDel, function (err, result) {

            if (err) {
                callback(err, null);
            } else {

                let ress = {
                    message: "Customer deleted successfully",
                    data: result,
                };
                callback(null, ress);
            }
        })
    },

}
