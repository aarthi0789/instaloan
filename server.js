const express = require('express')// requiring the express module function 
const InstaLoan = express(); // calling express modules function
const mongoose = require('mongoose');// requiring the mongoose 

const appConfig = require('./ConfigurationDB/appConfig');// requiring the app route connection
const dbConfig = require('./ConfigurationDB/dbConfig');// requiring the dbConfig connection
const Cust_Routes = require('./Routes/customers');// requiring Routes folder function
const Loan_Routes = require('./Routes/loan');// Requring Loan details from Routers
const repayment_Routes = require('./Routes/Repayments');// Requring Repayments details from Routers

InstaLoan.use(express.json())// using middleware(body_parse from express module)

// Basic API
InstaLoan.get('/', function (req, res) {
    res.send("Welcome to Insta Loan");
});





Cust_Routes(InstaLoan);// calling  Customer routes APIs function
Loan_Routes(InstaLoan);// calling  Customer Loans routes APIs function
repayment_Routes(InstaLoan);// calling  Customer repayments routes APIs function
InstaLoan.listen(appConfig.port);//calling appConfig file has port:3000
console.log(" This app is running on 3000");



// DB connect to robo 3t
// mongoose.connect(MongoDB_connection_String, Options_object, callback-function);
mongoose.connect(dbConfig.mongo_connection_Str, dbConfig.Options_object, function (err, res) {
    if (err) {
        console.log("\u001b[31m", "Mongo DB Not connected", err);
    } else {
        console.log("\u001b[36m", "Mongo DB Connected sucessfully");
    }
});

