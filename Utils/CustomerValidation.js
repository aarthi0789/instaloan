const Joi = require('joi');

module.exports = {

	// Customer registration req data validator schema.
	customer_reg: Joi.object().keys({
		firstName: Joi.string().required().min(3).max(6),
		lastName: Joi.string().required(),
		age: Joi.string().required(),
		gender: Joi.string().required().valid("male", "female", "others"),
		address: Joi.string().required(),
		mobile_num: Joi.string().required(),
		emailId: Joi.string().required(),
		occupation: Joi.string().required()
	}),


	customer_list: Joi.object().keys({
		firstName: Joi.string(),
		lastName: Joi.string(),
		age: Joi.string(),
		gender: Joi.string().max(100),
		address: Joi.string(),
		mobile_num: Joi.string(),
		emailId: Joi.string(),
		occupation: Joi.string()
	}),

	customer_update: Joi.object().keys({
		_id: Joi.string().required().min(24),
		// firstName: Joi.string().required().min(3).max(5),
		// lastName: Joi.string().required(),
		occupation: Joi.string().required(),
		// age: Joi.string().required(),
		// gender: Joi.string().required().valid("male", "female", "others"),
		// address: Joi.string().required(),
		mobile_num: Joi.string().required().max(9),
		emailId: Joi.string().required(),
	}),

	customer_delete: Joi.object().keys({
		_id: Joi.string().required().min(24)
	})

};