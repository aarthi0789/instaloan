const Joi = require('joi');

module.exports = {

    loan_reg: Joi.object().keys({
        _id: Joi.string().required(),
        LoanType: Joi.string().required().min(3).max(10),
        LoanAmount: Joi.string().required(),
        customerId: Joi.string().required().min(24).max(26),
        payment_mode: Joi.string().required(),

    }),

    loan_Valist: Joi.object().keys({
        _id: Joi.string(),
        LoanType: Joi.string(),
        LoanAmount: Joi.string(),
        customerId: Joi.string(),
        payment_mode: Joi.string(),
        release_date: Joi.string()
    }),

    loan_Valupdate: Joi.object().keys({
        _id: Joi.string().required(),
        // LoanType: Joi.string().required().min(3).max(10),
        LoanAmount: Joi.string().required(),
        // customerId: Joi.string().required().min(24).max(26),
        payment_mode: Joi.string().required(),

    }),
    loan_Valdelete: Joi.object().keys({
        _id: Joi.string().required().min(24).max(26)
    })

}