

module.exports = {

    sendEmail: function (email, callback) {
        console.log("In Util/Universal Functions, 'Send Email' function");
        callback(null, "Email sent successfully.");
    },

    // creating a save document in SaveInDB function

    saveInDB: function (modelName, saveObject, callback) {
        console.log("Utils/UnviversalFunctions: Inside 'SaveInDB' function.");
        modelName(saveObject).save(function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, res);
            }
        });
    },

    // Generic function to list the documents based on the queryObject from the given modelName.
    list_documents_in_DB: function (modelName, saveObject, callback) {
        console.log("Utils/UniversalFunctions: Inside 'list_documents_in_DB' function");

        modelName.find(queryObj, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, res);
            }
        });
    },
    // Generic function to update the documents based on the queryObject from the given modelName.
    update_documents_inDB: function (modelName, queryObj, updateObj, callback) {
        console.log("Utils/UniversalFunctions: Inside 'update_documents_inDB' functions", modelName, queryObj, updateObj);
        modelName.findOneAndUpdate(queryObj, updateObj, function (err, res) {
            console.log("DB response is ----", err, res);
            if (err) {
                callback(err, null);
            } else {
                callback(null, res);
            }
        })
    },

    delete_document_in_DB: function (modelName, queryObj, callback) {
        console.log("Utils/UniversalFunctions: Inside 'delete_document_in_DB' functions");
        modelName.deleteOne(queryObj, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, res);
            }
        });
    }
}